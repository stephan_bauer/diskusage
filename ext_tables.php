<?php
defined('TYPO3') or die();

// Register the backend module
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
    'MEDIAESSENZ.Diskusage',
    'file',
    'diskusage',
    '',
    [
        \MEDIAESSENZ\Diskusage\Controller\BackendModuleController::class => 'default',
    ],
    [
        'access' => 'admin',
        'icon' => 'EXT:diskusage/Resources/Public/Icons/Extension.svg',
        'labels' => 'LLL:EXT:diskusage/Resources/Private/Language/locallang.xlf',
        'navigationComponentId' => 'TYPO3/CMS/Backend/Tree/FileStorageTreeContainer',
        /* Comment the next two lines to disable navigation frame */
        //'navigationComponentId' => '',
        //'inheritNavigationComponentFromMainModule' => false,
    ]
);
