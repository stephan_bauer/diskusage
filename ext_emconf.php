<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "diskusage".
 *
 * Auto generated 26-03-2018 12:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['diskusage'] = [
    'title' => 'Disk Usage',
    'description' => 'Extension to visualize disk usage of TYPO3 storages',
    'version' => '3.0.0',
    'state' => 'stable',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '11.5.0-12.4.99'
                ],
        ],
    'autoload' =>
        [
            'psr-4' =>
                [
                    'MEDIAESSENZ\\Diskusage\\' => 'Classes',
                ],
        ]
];
