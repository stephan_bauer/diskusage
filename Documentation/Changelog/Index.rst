﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt

ChangeLog
---------

- v.1.0.0: Initial Release
- v.1.2.0: Make TYPO3 10 compatible; Update js libraries
- v.1.2.1: Fix typo in documentation
