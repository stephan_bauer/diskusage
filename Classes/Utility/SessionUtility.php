<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class SessionUtility implements \TYPO3\CMS\Core\SingletonInterface
{
  /**
   * @var string
   */
  protected $moduleKey = 'diskusage';

  /**
   * Returns a class instance.
   *
   * @return SessionUtility
   */
  static public function getInstance(): SessionUtility
  {
    return GeneralUtility::makeInstance(SessionUtility::class);
  }

  public function __construct()
  {
    // Initialize storage from the current
    if (!is_array(SystemUtility::getBackendUserAuthentication()->uc['moduleData'][$this->moduleKey])) {
      SystemUtility::getBackendUserAuthentication()->uc['moduleData'][$this->moduleKey] = [];
      SystemUtility::getBackendUserAuthentication()->writeUC();
    }
  }

  /**
   * Return a value from the Session according to the key
   *
   * @param string $key
   *
   * @return mixed
   */
  public function get($key)
  {
    return SystemUtility::getBackendUserAuthentication()->uc['moduleData'][$this->moduleKey][$key];
  }

  /**
   * Set a key to the Session.
   *
   * @param string $key
   * @param mixed $value
   *
   * @return void
   */
  public function set($key, $value): void
  {
    SystemUtility::getBackendUserAuthentication()->uc['moduleData'][$this->moduleKey][$key] = $value;
    SystemUtility::getBackendUserAuthentication()->writeUC();
  }
}
