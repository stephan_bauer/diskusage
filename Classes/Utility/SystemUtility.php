<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Utility;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Localization\LanguageService;

class SystemUtility
{
  /**
   * @return BackendUserAuthentication
   */
  public static function getBackendUserAuthentication(): BackendUserAuthentication
  {
    return $GLOBALS['BE_USER'];
  }

  /**
   * @return LanguageService
   */
  public static function getLanguageService(): LanguageService
  {
    return $GLOBALS['LANG'];
  }

}
