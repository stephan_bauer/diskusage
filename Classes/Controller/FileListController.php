<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Controller;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Filelist\Type\ViewMode;

class FileListController extends \TYPO3\CMS\Filelist\Controller\FileListController
{
    /**
     * Registers the Icons into the docheader
     *
     * @throws InvalidArgumentException
     * @throws RouteNotFoundException
     */
    protected function registerAdditionalDocHeaderButtons(ServerRequestInterface $request): void
    {
        parent::registerAdditionalDocHeaderButtons($request);

        if ($this->getBackendUser()->isAdmin()) {

            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

            if ((new Typo3Version())->getMajorVersion() < 12) {
                $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();
            } else {
                $buttonBar = $this->view->getDocHeaderComponent()->getButtonBar();
            }

            $routeIdentifier = (new Typo3Version())->getMajorVersion() < 12 ? 'file_DiskusageDiskusage' : 'file_diskusage';
            $diskusageButton = $buttonBar->makeLinkButton()
                ->setHref((string)$uriBuilder->buildUriFromRoute(
                    $routeIdentifier,
                    [
                        'id' => $this->folderObject->getCombinedIdentifier(),
                        'returnUrl' => (new Typo3Version())->getMajorVersion() < 12 ? $this->filelist->listURL() : $this->filelist->createModuleUri(['viewMode' => ViewMode::LIST->value]),
                    ]
                ))
                ->setTitle($this->getLanguageService()->sL('LLL:EXT:diskusage/Resources/Private/Language/locallang.xlf:diskusage_backend_module.docheader.menu.action.main.title'))
                ->setIcon($this->iconFactory->getIcon('actions-viewmode-photos', Icon::SIZE_SMALL));
            $buttonBar->addButton($diskusageButton, ButtonBar::BUTTON_POSITION_LEFT, 4);
        }
    }
}
