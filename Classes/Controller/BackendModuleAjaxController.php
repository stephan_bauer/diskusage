<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Controller;

use MEDIAESSENZ\Diskusage\Utility\SessionUtility;
use MEDIAESSENZ\Diskusage\Service\DiskUsageService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class BackendModuleAjaxController
{
    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     * @throws \TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException
     */
    public function defaultAction(ServerRequestInterface $request): ResponseInterface
    {
        $combinedIdentifierOfCurrentFolder = SessionUtility::getInstance()->get('combinedIdentifierOfCurrentFolder');
        $currentFolder = GeneralUtility::makeInstance(ResourceFactory::class)->getFolderObjectFromCombinedIdentifier($combinedIdentifierOfCurrentFolder);
        $currentFolderWithArrows = str_replace('/', ' ⟩ ',
            rtrim($currentFolder->getStorage()->getPublicUrl($currentFolder), '/'));
        $hideReferences = SessionUtility::getInstance()->get('hideReferences') ?? false;

        $response = new Response();
        $response->getBody()->write(json_encode([
            'folder' => $currentFolderWithArrows,
            'diskusage' => DiskUsageService::getData($currentFolder, $hideReferences),
            'hideReferences' => $hideReferences,
        ]));

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function hideReferencedFilesAction(
        ServerRequestInterface $request
    ): ResponseInterface {
        $requestProperties = json_decode(file_get_contents('php://input'), true);
        $postedParams = $requestProperties ?? [];
        if (!isset($postedParams['hideReferences'])) {
            $postedParams['hideReferences'] = SessionUtility::getInstance()->get('hideReferences') ?? false;
        } else {
            SessionUtility::getInstance()->set('hideReferences', (bool)$postedParams['hideReferences']);
        }
        $response = new Response();
        $response->getBody()->write(json_encode($postedParams));

        return $response;
    }

    /**
     * @return BackendUserAuthentication
     */
    protected static function getBackendUserAuthentication(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

}
